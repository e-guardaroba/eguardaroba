import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { RegistrationModalComponent } from '../registration-modal/registration-modal.component';
import { LoginModalComponent } from '../login-modal/login-modal.component';

@Component({
  selector: 'nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.less']
})
export class NavBarComponent implements OnInit {

  constructor(private _modalService: NgbModal) { }
  isLoggedIn = false;
  theModalRef = this._modalService;

  ngOnInit() { }
  openModal(event, modalType) {
    event.preventDefault();
    var component;
    if (modalType === 'Registration') component = RegistrationModalComponent;
    else component = LoginModalComponent;
    this.theModalRef.open(component, {
      size: 'lg'
    });
  };

}