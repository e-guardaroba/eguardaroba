import { Component, OnInit } from '@angular/core';
import { OutfitOptionsComponent } from "../outfit-options/outfit-options.component";
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'outfits',
  templateUrl: './outfits.component.html',
  styleUrls: ['./outfits.component.less']
})
export class OutfitsComponent implements OnInit {
  constructor(private _modalService: NgbModal) { }
  theModalRef = this._modalService;

  ngOnInit() { }
  openModal(event, modalType) {
    event.preventDefault();
    var component;
    if (modalType === 'Options') component = OutfitOptionsComponent;
    this.theModalRef.open(component, {
      size: 'lg'
    });
  };
}
