import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'feed',
  templateUrl: './feed.component.html',
  styleUrls: ['./feed.component.less']
})
export class FeedComponent implements OnInit {
  Posts = [
    { title: 'Post1', text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas venenatis imperdiet viverra. Aliquam finibus accumsan ipsum ut aliquet. Maecenas accumsan id lacus nec venenatis." },
    { title: 'Post2', text: "Quisque eget enim convallis, euismod quam ut, dapibus purus. Mauris dignissim, ante eget suscipit malesuada, lectus ante maximus sapien, nec maximus tortor orci sit amet lacus. " },
    { title: 'Post3', text: "enean eleifend ac odio vitae auctor. Suspendisse eget commodo orci, id eleifend mauris. Duis finibus, diam sit amet congue rhoncus, quam lectus fringilla ipsum, a varius metus mauris eget est. " },
    { title: 'Post4', text: "Mauris dignissim, ante eget suscipit malesuada, lectus ante maximus sapien." },
  ]
  Adds = [
    { title: 'Add1', text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas venenatis imperdiet viverra. Aliquam finibus accumsan ipsum ut aliquet. Maecenas accumsan id lacus nec venenatis." },
    { title: 'Add2', text: "Quisque eget enim convallis, euismod quam ut, dapibus purus. Mauris dignissim, ante eget suscipit malesuada, lectus ante maximus sapien, nec maximus tortor orci sit amet lacus. " },
    { title: 'Add3', text: "Enean eleifend ac odio vitae auctor. Suspendisse eget commodo orci, id eleifend mauris. Duis finibus, diam sit amet congue rhoncus, quam lectus fringilla ipsum, a varius metus mauris eget est. " },
  ]
  constructor() {
  }

  ngOnInit() {
  }

}
