import { Component, OnInit } from '@angular/core';
import { NgbModalConfig, NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'login-modal',
  templateUrl: './login-modal.component.html',
  styleUrls: ['./login-modal.component.less']
})
export class LoginModalComponent implements OnInit {

  ngOnInit() { }
  constructor(public activeModal: NgbActiveModal) {
    // customize default values of modals used by this component tree
  }
  closeModal() {
    this.activeModal.close('Close click');
  }
}
