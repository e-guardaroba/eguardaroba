import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatStepperModule, MatInputModule, MatButtonModule, MatAutocompleteModule, MatNativeDateModule, MatDatepickerModule, MatCheckboxModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { RegistrationModalComponent } from './registration-modal/registration-modal.component';
import { HomeComponent } from './home/home.component';
import { WardrobeComponent } from './wardrobe/wardrobe.component';
import { ClothesComponent } from './clothes/clothes.component';
import { LoginModalComponent } from './login-modal/login-modal.component';
import { FeedComponent } from './feed/feed.component';
import { SuitcaseComponent } from './suitcase/suitcase.component';
import { SuitcaseOptionsComponent } from './suitcase-options/suitcase-options.component';
import { ClothCardComponent } from './cloth-card/cloth-card.component';
import { OutfitOptionsComponent } from './outfit-options/outfit-options.component';
import { OutfitsComponent } from './outfits/outfits.component';
import { ClothOptionsComponent } from './cloth-options/cloth-options.component';
import { SafePipe } from './safe.pipe';
import { SuitcaseNonAutoOptionsComponent } from './suitcase-non-auto-options/suitcase-non-auto-options.component';
import { CompanyComponent } from './company/company.component';
import { CreateAdOptionsComponent } from './create-ad-options/create-ad-options.component';

@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    RegistrationModalComponent,
    HomeComponent,
    WardrobeComponent,
    ClothesComponent,
    LoginModalComponent,
    FeedComponent,
    SuitcaseComponent,
    SuitcaseOptionsComponent,
    ClothCardComponent,
    ClothOptionsComponent,
    OutfitOptionsComponent,
    OutfitsComponent,
    SafePipe,
    SuitcaseNonAutoOptionsComponent,
    CompanyComponent,
    CreateAdOptionsComponent,
  ],
  entryComponents: [
    RegistrationModalComponent,
    LoginModalComponent,
    SuitcaseOptionsComponent,
    OutfitOptionsComponent,
    ClothOptionsComponent,
    SuitcaseNonAutoOptionsComponent,
    CreateAdOptionsComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    MatStepperModule,
    MatInputModule,
    MatButtonModule,
    MatAutocompleteModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatNativeDateModule,
    MatDatepickerModule,
    HttpClientModule,
    MatCheckboxModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
