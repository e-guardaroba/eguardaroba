import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClothCardComponent } from './cloth-card.component';

describe('ClothCardComponent', () => {
  let component: ClothCardComponent;
  let fixture: ComponentFixture<ClothCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClothCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClothCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
