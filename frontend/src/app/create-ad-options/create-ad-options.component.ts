import { Component, OnInit } from '@angular/core';
import { NgbModalConfig, NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CompanyService } from '../company.service';

@Component({
  selector: 'create-ad-options',
  templateUrl: './create-ad-options.component.html',
  styleUrls: ['./create-ad-options.component.less']
})

export class CreateAdOptionsComponent implements OnInit {
  isLinear = false;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  thirdFormGroup: FormGroup;
  fourthFormGroup: FormGroup;
  fifthFormGroup: FormGroup;

  constructor(public activeModal: NgbActiveModal, private _formBuilder: FormBuilder,
    private _companyApi: CompanyService) { }

  ngOnInit() {
    this.firstFormGroup = this._formBuilder.group({
      clothesCtrl: ['', Validators.required]
    });
    this.secondFormGroup = this._formBuilder.group({
      numberCtrl: ['', Validators.required]
    });
    this.thirdFormGroup = this._formBuilder.group({
      Startpicker: ['', Validators.required],
      Endpicker: ['', Validators.required]
    });
    this.fourthFormGroup = this._formBuilder.group({
      photoFileCtrl: ['', Validators.required]
    });
  }
  closeModal() {
    // call api.
    let AdDict = {
      'userId': 1,
      'clothes': this.firstFormGroup.value.clothesCtrl,
      'number': this.secondFormGroup.value.numberCtrl,
      'timePeriod': `${this.thirdFormGroup.value.Endpicker}-${this.thirdFormGroup.value.Startpicker}`,
    }
    alert(`Number of apearences:${AdDict.number}\nTime period:${AdDict.timePeriod}`);

    console.log(AdDict);
    this._companyApi.postAd(AdDict).subscribe(res => { console.log(res) }, err => {
      console.log(err);
    });
    this.activeModal.close('Close click');
  }
}