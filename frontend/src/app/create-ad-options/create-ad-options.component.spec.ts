import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { createAdOptionsComponent } from './create-ad-options.component';

describe('createAdOptionsComponent', () => {
  let component: createAdOptionsComponent;
  let fixture: ComponentFixture<createAdOptionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ createAdOptionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(createAdOptionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
