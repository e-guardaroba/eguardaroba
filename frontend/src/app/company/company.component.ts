import { Component, OnInit } from '@angular/core';
import { CreateAdOptionsComponent } from '../create-ad-options/create-ad-options.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'company',
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.less']
})
export class CompanyComponent implements OnInit {

  constructor(private _modalService: NgbModal) { }
  theModalRef = this._modalService;

  ngOnInit() {
  }
  openModal(event, modalType) {
    event.preventDefault();
    var component;
    if (modalType === 'Options') component = CreateAdOptionsComponent;
    this.theModalRef.open(component, {
      size: 'lg'
    });
  };

}
