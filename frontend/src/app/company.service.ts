import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CompanyService {

  constructor(private _http: HttpClient) { }

  postAd(adObj: Object) {
    let Suitcaseparams = new HttpParams()
      .set('user_id', '1245')
      .set('clothes', adObj['clothes'])
      .set('number', adObj['number'])
      .set('time_period', adObj['timePeriod'])
    return this._http.post('http://localhost:8080/api/company/ad', Suitcaseparams)
  }
}