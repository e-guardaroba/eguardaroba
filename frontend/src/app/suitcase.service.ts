import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SuitcaseService {

  constructor(private _http: HttpClient) { }

  postSuitcase(suitcaseObj: Object) {
    let Suitcaseparams = new HttpParams()
      .set('user_id', '1245')
      .set('name', suitcaseObj['name'])
      .set('time_period', suitcaseObj['timePeriod'])
      .set('occasion', suitcaseObj['occasion'])
      .set('airline', suitcaseObj['airline'])
      .set('specs', suitcaseObj['specs'])
      .set('clothes', suitcaseObj['clothes'])
    return this._http.post('http://localhost:8080/api/suitcase/', Suitcaseparams)
  }

}
