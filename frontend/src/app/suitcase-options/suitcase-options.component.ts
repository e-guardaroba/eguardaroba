import { Component, OnInit } from '@angular/core';
import { NgbModalConfig, NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SuitcaseService } from '../suitcase.service';

@Component({
  selector: 'suitcase-options',
  templateUrl: './suitcase-options.component.html',
  styleUrls: ['./suitcase-options.component.less']
})
export class SuitcaseOptionsComponent implements OnInit {
  isLinear = false;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  thirdFormGroup: FormGroup;
  fourthFormGroup: FormGroup;
  fifthFormGroup: FormGroup;

  constructor(public activeModal: NgbActiveModal, private _formBuilder: FormBuilder,
    private _suitcaseApi: SuitcaseService) { }

  ngOnInit() {
    this.firstFormGroup = this._formBuilder.group({
      nameCtrl: ['', Validators.required]
    });
    this.secondFormGroup = this._formBuilder.group({
      clothesCtrl: ['', Validators.required]
    });
    this.thirdFormGroup = this._formBuilder.group({
      Startpicker: ['', Validators.required],
      Endpicker: ['', Validators.required]
    });
    this.fourthFormGroup = this._formBuilder.group({
      occasionCtrl: ['', Validators.required]
    });
    this.fifthFormGroup = this._formBuilder.group({
      airlineCtrl: ['', Validators.required]
    });
  }
  closeModal() {
    // call api.
    let suitcaseDict = {
      'userId': 1,
      'name': this.firstFormGroup.value.nameCtrl,
      'timePeriod': `${this.thirdFormGroup.value.Endpicker}-${this.thirdFormGroup.value.Startpicker}`,
      'occasion': this.fourthFormGroup.value.occasionCtrl,
      'airline': this.fifthFormGroup.value.airline,
      'specs': 'hello I am a spec',
      'clothes': this.secondFormGroup.value.clothesCtrl
    }
    console.log(suitcaseDict);
    this._suitcaseApi.postSuitcase(suitcaseDict).subscribe(res => { console.log(res) }, err => {
      console.log(err);
    });
    this.activeModal.close('Close click');
  }
}