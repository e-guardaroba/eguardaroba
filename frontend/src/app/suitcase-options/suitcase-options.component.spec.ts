import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuitcaseOptionsComponent } from './suitcase-options.component';

describe('SuitcaseOptionsComponent', () => {
  let component: SuitcaseOptionsComponent;
  let fixture: ComponentFixture<SuitcaseOptionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuitcaseOptionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuitcaseOptionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
