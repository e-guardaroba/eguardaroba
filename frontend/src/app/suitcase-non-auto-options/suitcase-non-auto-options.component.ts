import { Component, OnInit } from '@angular/core';
import { NgbModalConfig, NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SuitcaseService } from '../suitcase.service';

@Component({
  selector: 'app-suitcase-non-auto-options',
  templateUrl: './suitcase-non-auto-options.component.html',
  styleUrls: ['./suitcase-non-auto-options.component.less']
})
export class SuitcaseNonAutoOptionsComponent implements OnInit {
  isLinear = false;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  thirdFormGroup: FormGroup;

  constructor(public activeModal: NgbActiveModal, private _formBuilder: FormBuilder,
    private _suitcaseApi: SuitcaseService) { }

  ngOnInit() {
    this.firstFormGroup = this._formBuilder.group({
      nameCtrl: ['', Validators.required]
    });
    this.secondFormGroup = this._formBuilder.group({
      clothesCtrl: ['', Validators.required]
    });
    this.thirdFormGroup = this._formBuilder.group({
      Startpicker: ['', Validators.required],
      Endpicker: ['', Validators.required]
    });
  }
  closeModal() {
    // call api.
    let suitcaseDict = {
      'userId': 1,
      'name': this.firstFormGroup.value.nameCtrl,
      'timePeriod': `${this.thirdFormGroup.value.Endpicker}-${this.thirdFormGroup.value.Startpicker}`,
      'clothes': this.secondFormGroup.value.clothesCtrl,
      'occasion': 'Manualy_Generated',
      'airline': 'NO_AIRLINE',
      'specs': 'hello I am a spec',
    }
    console.log(suitcaseDict);
    this._suitcaseApi.postSuitcase(suitcaseDict).subscribe(res => { console.log(res) }, err => {
      console.log(err);
    });
    this.activeModal.close('Close click');
  }

}
