import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuitcaseNonAutoOptionsComponent } from './suitcase-non-auto-options.component';

describe('SuitcaseNonAutoOptionsComponent', () => {
  let component: SuitcaseNonAutoOptionsComponent;
  let fixture: ComponentFixture<SuitcaseNonAutoOptionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuitcaseNonAutoOptionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuitcaseNonAutoOptionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
