import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { HomeComponent } from "./home/home.component";
import { WardrobeComponent } from "./wardrobe/wardrobe.component";
import { ClothesComponent } from "./clothes/clothes.component";
import { FeedComponent } from "./feed/feed.component";
import { SuitcaseComponent } from "./suitcase/suitcase.component";
import { OutfitsComponent } from './outfits/outfits.component';
import { CompanyComponent } from './company/company.component';

const routes: Routes = [
  { path: "home", component: HomeComponent },
  { path: "wardrobe", component: WardrobeComponent },
  { path: "clothes", component: ClothesComponent },
  { path: "feed", component: FeedComponent },
  { path: "suitcase", component: SuitcaseComponent },
  { path: "outfits", component: OutfitsComponent },
  { path: "company", component: CompanyComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
