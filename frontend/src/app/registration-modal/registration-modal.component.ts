import { Component, OnInit } from '@angular/core';
import { NgbModalConfig, NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'registration-modal',
  templateUrl: './registration-modal.component.html',
  styleUrls: ['./registration-modal.component.less'],
  // add NgbModalConfig and NgbModal to the component providers
  providers: [NgbModalConfig, NgbModal]
})
export class RegistrationModalComponent implements OnInit {

  ngOnInit() { }
  constructor(public activeModal: NgbActiveModal) {
    // customize default values of modals used by this component tree
  }
  closeModal() {
    this.activeModal.close('Close click');
  }
}
