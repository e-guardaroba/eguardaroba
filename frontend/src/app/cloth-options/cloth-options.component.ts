import { Component, OnInit } from '@angular/core';
import { NgbModalConfig, NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ClothService } from '../cloth.service';

@Component({
  selector: 'cloth-options',
  templateUrl: './cloth-options.component.html',
  styleUrls: ['./cloth-options.component.less']
})
export class ClothOptionsComponent implements OnInit {
  isLinear = false;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  thirdFormGroup: FormGroup;
  fourthFormGroup: FormGroup;
  fifthFormGroup: FormGroup;
  selectedFile: File = null;
  fileName: string = "";

  constructor(public activeModal: NgbActiveModal, private _formBuilder: FormBuilder,
    private _clothApi: ClothService) { }

  ngOnInit() {
    this.firstFormGroup = this._formBuilder.group({
      nameCtrl: ['', Validators.required]
    });
    this.secondFormGroup = this._formBuilder.group({
      clothTypeCtrl: ['', Validators.required]
    });
    this.thirdFormGroup = this._formBuilder.group({
      photoCtrl: ['', Validators.required]
    });
  }
  closeModal() {
    // call api.
    let clothDict = {
      'clothId': 1,
      'wardrobeId': 1,
      'name': this.firstFormGroup.value.nameCtrl,
      'clothType': this.secondFormGroup.value.clothTypeCtrl,
      'photo': this.selectedFile
    }
    console.log(clothDict);
    this._clothApi.postCloth(clothDict).subscribe(res => { console.log(res) }, err => {
      console.log(err);
    });
    this.activeModal.close('Close click');
  }

  onFileSelected(event) {
    this.selectedFile = this._clothApi.FileSelected(event);
    this.fileName = this.selectedFile.name;
  }
}
