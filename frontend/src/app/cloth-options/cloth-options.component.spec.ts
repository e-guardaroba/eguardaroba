import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClothOptionsComponent } from './cloth-options.component';

describe('ClothOptionsComponent', () => {
  let component: ClothOptionsComponent;
  let fixture: ComponentFixture<ClothOptionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClothOptionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClothOptionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
