import { TestBed } from '@angular/core/testing';

import { SuitcaseService } from './suitcase.service';

describe('SuitcaseService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SuitcaseService = TestBed.get(SuitcaseService);
    expect(service).toBeTruthy();
  });
});
