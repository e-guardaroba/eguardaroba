import { Component, OnInit } from '@angular/core';
import { ClothOptionsComponent } from "../cloth-options/cloth-options.component";
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { ClothService } from '../cloth.service';
// import { SafePipe } from '../safe.pipe';

@Component({
  selector: 'clothes',
  templateUrl: './clothes.component.html',
  styleUrls: ['./clothes.component.less']
})
export class ClothesComponent implements OnInit {
  // constructor(private _modalService: NgbModal) { }
  theModalRef = this._modalService;

  // ngOnInit() { }
  // openModal(event, modalType) {
  //   event.preventDefault();
  //   var component;
  //   if (modalType === 'Options') component = ClothOptionsComponent;
  //   this.theModalRef.open(component, {
  //     size: 'lg'
  //   });
  // };
  clothesArray: any;
  constructor(private _modalService: NgbModal,private _clothApi: ClothService, private _sanitizer: DomSanitizer) {
    this.getClothes();
  }

  async getClothes() {
    this.clothesArray = await this._clothApi.getAllClothes();
    // let photo = this.clothesArray[0].photo.toString();
    // this.getClothImage(photo);
  }

  ngOnInit() {
    // this._clothApi.getAllClothes().subscribe(res => {
    //   // this.clothesArray = res;
    //   console.log(res);
    // });

  };

  public getClothImage(cloth: any) {
    // var clothPhoto: SafeUrl;
    // clothPhoto = this._sanitizer.bypassSecurityTrustUrl(cloth.photo);
    // return this._sanitizer.bypassSecurityTrustUrl(cloth.photo.replace(/\\/g, '/'));
    // let imagePath = cloth.photo.toString();
    // console.log(this._safe.transform(cloth, 'resourceUrl'));

  };

  openModal(event, modalType) {
    event.preventDefault();
    var component;
    if (modalType === 'Options') component = ClothOptionsComponent;
    this.theModalRef.open(component, {
      size: 'lg'
    });
  };

}
