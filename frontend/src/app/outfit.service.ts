import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class OutfitService {

  constructor(private _http: HttpClient) { }

  postOutfit(outfitObj: Object) {
    let outfitParams = new HttpParams()
      .set('user_id', '125')
      .set('name', 'qwgwq')
      .set('weather', 'false')
      .set('clothes', '1,5')
    return this._http.post('http://localhost:8080/api/outfit/', outfitParams)
  }

}
