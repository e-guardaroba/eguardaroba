import { Component, OnInit } from "@angular/core";
import { SuitcaseOptionsComponent } from "../suitcase-options/suitcase-options.component";
import { SuitcaseNonAutoOptionsComponent } from "../suitcase-non-auto-options/suitcase-non-auto-options.component";
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: "suitcase",
  templateUrl: "./suitcase.component.html",
  styleUrls: ["./suitcase.component.less"]
})
export class SuitcaseComponent implements OnInit {
  constructor(private _modalService: NgbModal) { }
  theModalRef = this._modalService;

  ngOnInit() { }
  openModal(event, modalType) {
    event.preventDefault();
    var component;
    if (modalType === 'Options') component = SuitcaseOptionsComponent;
    if (modalType === 'NonAutoOptions') component = SuitcaseNonAutoOptionsComponent;
    this.theModalRef.open(component, {
      size: 'lg'
    });
  };
}
