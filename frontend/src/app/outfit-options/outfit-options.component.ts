import { Component, OnInit } from '@angular/core';
import { NgbModalConfig, NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { OutfitService } from '../outfit.service';

@Component({
  selector: 'outfit-options',
  templateUrl: './outfit-options.component.html',
  styleUrls: ['./outfit-options.component.less']
})
export class OutfitOptionsComponent implements OnInit {
  isLinear = false;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  thirdFormGroup: FormGroup;


  constructor(public activeModal: NgbActiveModal, private _formBuilder: FormBuilder,
    private _outfitApi: OutfitService) { }

  ngOnInit() {
    this.firstFormGroup = this._formBuilder.group({
      nameCtrl: ['', Validators.required]
    });
    this.secondFormGroup = this._formBuilder.group({
      weatherCtrl: ['false', Validators.required],
      favoriteClothesCtrl: ['', Validators.required],
      seasonCtrl: ['', Validators.required]
    });
    this.thirdFormGroup = this._formBuilder.group({
      clothesCtrl: ['', Validators.required]
    });
  }
  closeModal() {
    // call api.
    let outfitDict = {
      'userId': 1,
      'name': this.firstFormGroup.value.nameCtrl,
      'useWeather': false,
      'useFavorites': this.secondFormGroup.value.favoriteClothesCtrl,
      'useSeason': this.secondFormGroup.value.seasonCtrl,
      'clothes': this.thirdFormGroup.value.clothesCtrl
    }
    console.log(outfitDict);
    this._outfitApi.postOutfit(outfitDict).subscribe(res => { console.log(res) }, err => {
      console.log(err);
      console.log(outfitDict)
    });
    this.activeModal.close('Close click');
  }
}