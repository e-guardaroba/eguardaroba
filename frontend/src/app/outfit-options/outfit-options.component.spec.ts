import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OutfitOptionsComponent } from './outfit-options.component';

describe('OutfitOptionsComponent', () => {
  let component: OutfitOptionsComponent;
  let fixture: ComponentFixture<OutfitOptionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OutfitOptionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OutfitOptionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
