import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ClothService {

  selectedFile: File = null;

  constructor(private _http: HttpClient) { }

  postCloth(clothObj: Object) {
    let Clothparams = new HttpParams()
      .set('cloth_id', '1')
      .set('wardrobe_id', '1')
      .set('name', clothObj['name'])
      .set('cloth_type', clothObj['clothType'])
      .set('photo', clothObj['photo'])
    return this._http.post('http://localhost:8080/api/cloth/', Clothparams)
  }

  async getAllClothes() {
    let clothesPromise = await this._http.get("http://localhost:8080/api/cloth/all").toPromise();
    let clothes = Promise.resolve(clothesPromise);
    return clothes;
  }

  FileSelected(event) {
    this.selectedFile = <File>event.target.files[0];
    console.log(this.selectedFile);
    return this.selectedFile;
  }
}