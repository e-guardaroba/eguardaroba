# Frontend

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.1.

## Requirements
For downloading the project dependencies you will need [npm](https://www.npmjs.com/) and [node](https://nodejs.org/en/) 10.x.x. Follow the links for more instrunctions on downloading the previous programs.

## Installation
For installing the frontend dependecies, navigate to frontend folder and run `npm install`.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Project Structure
For the project's frontend, Angular framework was used. __Frontend/src/app__ contains all the main functional code. It's file structure is explained below.

---
> ### Folders
> Each folder under `frontend/src/app` is an Angular component containing a html, css (less), ts and spec.ts file.
> All of the main code is in `.html`, `.less` and `.ts` files, and the tests are in `.spec.ts` files.

---
> ### Services
> Services are modules responsible for the API communication. All `.service.ts` files contain code, for requesting data from the api services. The `.service.spec.ts` files are responsible for testing the previous files.

---
> ### APP MODULE
> The `app.module.ts` module contains all the individual components and the imported libraries.

---
> ### ROUTING MODULE
> The `app-routing.module.ts` is the module responsible for declaring all webpage routes.
