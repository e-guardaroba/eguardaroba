# Welcome to the e-Guardaroba project.

## Getting Started
E-Guardaroba enables you to find the perfect outfit everyday and watch you entire wardrobe electronicaly. This is a school project, created by Xynos Anastasios, Poulis Iakovos, Terezakis Michail, Tsatsoulis Xristoforos and Tsoulos Georgios.


## Installation Instructions

Backend
* https://gitlab.com/e-guardaroba/eguardaroba/tree/master/backend
  
Frontend

* https://gitlab.com/e-guardaroba/eguardaroba/tree/master/frontend