""" Cloth rest api module. """
# Python Libs
import logging
import werkzeug
from flask_restplus import Resource, Namespace, reqparse
from flask import (request, jsonify)
# Project Files.
from config import CONFIGURATION
from . import lib
from models.guardrobe import Cloth as cloth_db_model

NAMESPACE = Namespace(
    'cloth', description='Api namespace representing a cloth.')
cloth_model = NAMESPACE.parser()
cloth_model.add_argument('wardrobe_id', type=int,
                         help='Wardrobe id where cloth belong to.')
cloth_model.add_argument('name', type=str, help='Cloth name.')
cloth_model.add_argument('type', type=str, help='Cloth type.')
cloth_model.add_argument(
    'photo', type=werkzeug.datastructures.FileStorage, location='files',
    help='Cloth type.')


@NAMESPACE.route('/')
class Cloth(Resource):
    """
    Api class for Clothes.
    """
    @NAMESPACE.expect(cloth_model)
    def post(self):
        """ Add new cloth to database. """
        args = request.args
        photo = cloth_model.parse_args()['photo']
        file_path = lib.save_file(photo)
        new_cloth = cloth_db_model(wardrobe_id=args['wardrobe_id'],
                                   name=args['name'], cloth_type=args['type'],
                                   photo=file_path)
        new_cloth.store_to_db()
        return args


@NAMESPACE.route('/all')
class Cloth(Resource):
    """
    Api class for Clothes.
    """

    def get(self):
        """ Get all clothes from database """
        clothes = lib.get_all_clothes()
        print(clothes)
        return jsonify(clothes)
