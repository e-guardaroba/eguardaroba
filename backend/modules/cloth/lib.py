""" Helper module for cloth rest api. """
# Python libs.
import os
import werkzeug
# Project files.
from models import session
from models.guardrobe import Cloth

IMAGES_PATH = os.path.realpath('images')


def save_file(file):
    """ Store file to frontend assets. """
    file_name = werkzeug.utils.secure_filename(file.filename)
    file_path = f'{IMAGES_PATH}/{file.name}.jpeg'
    file.save(file_path)

    return file_path


def get_all_clothes():
    """ Return all clothes from database. """
    # Get class attributes.
    table_attrs = Cloth.__table__.columns.keys()
    clothes = list()
    for class_instance in session.query(Cloth).all():
        clothe_dict = dict()
        for table_attr in table_attrs:
            clothe_dict[table_attr] = vars(class_instance)[table_attr]
        clothes.append(clothe_dict)
    session.close()
    return clothes
