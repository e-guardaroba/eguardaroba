""" Module containing helper funtions for the outfit api. """


def check_weather(weather):
    """ Online weather check. """
    weather = "sunny" if weather == 'true' else 'unknown'

    return weather


def generate_outfit(weather, cloth_ids):
    """ Generate outfit with AI. """
    cloth_ids = f"1, 5, 6, {cloth_ids}"
    if weather:
        cloth_ids = f"1, 2, 5, 6, {cloth_ids}"

    return cloth_ids
