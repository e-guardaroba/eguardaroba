""" Rest Api module for outfit functionality. """
# Python Libs
import logging
import json
from flask_restplus import Resource, Namespace, reqparse
from flask import (request, jsonify)
# Project Files.
from config import CONFIGURATION
from . import lib
from models.guardrobe import Outfit as OutfitModel


LOGGER = CONFIGURATION.get_logger(__name__)
NAMESPACE = Namespace(
    'outfit', description='Api namespace representing an outfit.')
outfit_model = NAMESPACE.parser()
outfit_model.add_argument('clothes', type=str, help='Cloth ids.')
outfit_model.add_argument('user_id', type=int, help='User\'s id')
outfit_model.add_argument('name', type=str, help='Outfit\'s name.')
outfit_model.add_argument(
    'weather', type=bool,
    help='True if weather needs to be checked before creating the outfit.')


@NAMESPACE.route('/')
class Outfit(Resource):
    """
    Api class for adding new outfit.
    """
    @NAMESPACE.expect(outfit_model)
    def post(self):
        """ Add new outfit to database. """
        args = request.args
        # Check if the weather needs to be considered.
        weather = lib.check_weather(args['weather'])

        clothe_ids = lib.generate_outfit(weather, args['clothes'])
        # Store outfit to db.
        new_outift = OutfitModel(clothes=clothe_ids,
                                 user_id=args['user_id'],
                                 name=args['name'],
                                 weather=weather)
        new_outift.store_to_db()

        return args
