""" Users rest api module. """
# Python Libs
import logging
from flask_restplus import Resource, Namespace, reqparse
from flask import (request, jsonify)
# Project Files.
from config import CONFIGURATION
from models.platform import User as UserModel

NAMESPACE = Namespace(
    'user', description='Api namespace representing an app user.')
user_model = NAMESPACE.parser()
user_model.add_argument('type', type=str, default='normal',
                        help='Type of user.', choices=['normal', 'admin'])
user_model.add_argument('username', type=str, help='User\'s username.')
user_model.add_argument('password', type=str, help='User\'s password.')
user_model.add_argument('email', type=str, help='User\'s email.')
user_model.add_argument('description', type=str,
                        help='A description provided by user.')
user_model.add_argument('wardrobes', type=str, help='User\'s wardrobes.')


@NAMESPACE.route('/')
class User(Resource):
    """
    Api class for adding new user.
    """
    @NAMESPACE.expect(user_model)
    def post(self):
        """ Add new user to database. """
        args = request.args
        new_user = UserModel(username=args['username'],
                             password=args['password'],
                             email=args['email'],
                             description=args['description'],
                             user_type=args['type'],
                             wardrobes=args['wardrobes'])
        new_user.store_to_db()
        return args


@NAMESPACE.route('/validation')
class User(Resource):
    """
    Api class for user validation
    """
    @NAMESPACE.param('username', 'User\'s name.')
    @NAMESPACE.param('password', 'User\'s password.')
    def get(self):
        """ Check if user exists in database. """
        args = request.args

        return args
