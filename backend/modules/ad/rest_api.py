""" Ad rest api module. """
# Python Libs
import logging
from flask_restplus import Resource, Namespace, reqparse
from flask import (request, jsonify)
# Project Files.
from config import CONFIGURATION
from . import lib
from models.platform import Ad as ad_db_model

NAMESPACE = Namespace(
    'ad', description='Api namespace representing a add.')
ad_model = NAMESPACE.parser()
ad_model.add_argument('user_id', type=int,
                      help='User id to whom ad belong to.')
ad_model.add_argument('time_period', type=str, help='Ad time-span.')
ad_model.add_argument('ad_number', type=int,
                      help='Ad number of occurencies.')


@NAMESPACE.route('/')
class Ad(Resource):
    """
    Api class for Ads.
    """
    @NAMESPACE.expect(ad_model)
    def post(self):
        """ Add new ad to database. """
        args = request.args
        new_add = ad_db_model(ad_number=args['ad_number'],
                              user_id=args['user_id'],
                              time_period=args['time_period'])
        new_add.store_to_db()

        return args
