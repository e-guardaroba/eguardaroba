""" Helper functions for suitcase rest api. """
# Project files.
from models import session
from models.guardrobe import Suitcase


def get_availability(period):
    """ Retunr true if availble or false otherwise. """
    period = period

    return True


def get_all_suitcases():
    """ Return all Suitcases from database. """
    # Get class attributes.
    table_attrs = Suitcase.__table__.columns.keys()
    Suitcasees = list()
    for class_instance in session.query(Suitcase).all():
        Suitcasee_dict = dict()
        for table_attr in table_attrs:
            Suitcasee_dict[table_attr] = vars(class_instance)[table_attr]
        Suitcasees.append(Suitcasee_dict)
    session.close()
    return Suitcasees
