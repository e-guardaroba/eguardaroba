""" Suitcase rest api module. """
# Python Libs
import logging
import werkzeug
from flask_restplus import Resource, Namespace, reqparse
from flask import (request, jsonify)
# Project Files.
from config import CONFIGURATION
from . import lib
from models.guardrobe import Suitcase as SuitcaseModel

NAMESPACE = Namespace(
    'suitcase', description='Api namespace representing a suitcase.')
suitcase_model = NAMESPACE.parser()
suitcase_model.add_argument('user_id', type=int,
                            help='User id where suitcase belongs to.')
suitcase_model.add_argument('name', type=str, help='Suitcase name.')
suitcase_model.add_argument('time_period', type=str,
                            help='Time period when suitcase will be used.')
suitcase_model.add_argument(
    'occasion', help='Occasion suitcase will be used for.')
suitcase_model.add_argument(
    'airline', help='Airline suiticase may be used to.')
suitcase_model.add_argument(
    'specs', help='Suitcase airline specifications.')
suitcase_model.add_argument(
    'clothes', help='Clothes suitcase contains.')


@NAMESPACE.route('/')
class Suitcase(Resource):
    """
    Api class for adding new suitcase.
    """
    @NAMESPACE.expect(suitcase_model)
    def post(self):
        """ Add new suitcase to database. """
        args = suitcase_model.parse_args()
        new_suitcase = SuitcaseModel(**args)
        new_suitcase.store_to_db()

        return args


@NAMESPACE.route('/all')
class Suitcases(Resource):
    """
    Api class for all suitcases.
    """

    def get(self):
        """ Get all suitcases database. """
        suitcases = lib.get_all_suitcases()

        return jsonify(suitcases)


@NAMESPACE.route('/check_availability')
class SuitcaseAvailability(Resource):
    """
    Api class for checking availability.
    """
    @NAMESPACE.param('time_period', 'Time period suitcase will be used.')
    def get(self):
        """
        Return true if suitcase is available for a specific time period
        and false otherwise.
        """
        time_period = request.args['time_period']
        availability = lib.get_availability(time_period)

        return availability
