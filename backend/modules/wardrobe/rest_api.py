""" Wardrobe rest api module. """
# Python Libs
import logging
import werkzeug
from flask_restplus import Resource, Namespace, reqparse
from flask import (request, jsonify)
# Project Files.
from config import CONFIGURATION
from models.guardrobe import Wardrobe as wardrobe_db_model

NAMESPACE = Namespace(
    'wardrobe', description='Api namespace representing a wardrobe.')
wardrobe_model = NAMESPACE.parser()
wardrobe_model.add_argument(
    'user_id', type=int, help='User\'s id to whom the wardrobe belongs to.')
wardrobe_model.add_argument('name', type=str, help='Wardrobe name.')
wardrobe_model.add_argument('description', type=str,
                            help='Wardrobe description.')
wardrobe_model.add_argument('area', type=str, help='Wadrobe location.')


@NAMESPACE.route('/')
class Wardrobe(Resource):
    """
    Api class for Wardrobes.
    """
    @NAMESPACE.expect(wardrobe_model)
    def post(self):
        """ Add new wardrobe to database. """
        args = request.args
        wadrobe_model = wardrobe_db_model(**args)
        wadrobe_model.store_to_db()
        return args
