# Python libs.
from flask_restplus import Resource, Api
from flask import Blueprint
# Project files.
from .user.rest_api import NAMESPACE as user_ns
from .cloth.rest_api import NAMESPACE as cloth_ns
from .suitcase.rest_api import NAMESPACE as suitcase_ns
from .wardrobe.rest_api import NAMESPACE as wardrobe_ns
from .outfit.rest_api import NAMESPACE as outfit_ns
from .ad.rest_api import NAMESPACE as ad_ns

API = Api(doc='/doc/', prefix='/api')

API.add_namespace(user_ns)
API.add_namespace(cloth_ns)
API.add_namespace(suitcase_ns)
API.add_namespace(wardrobe_ns)
API.add_namespace(outfit_ns)
API.add_namespace(ad_ns)
