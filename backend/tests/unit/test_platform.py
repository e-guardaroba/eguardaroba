""" Module containing unittests for platform module. """
# Python libs.
import pytest
from unittest import mock
# Project Files.
from models.platform import User


@pytest.fixture
def user_mock():
    """ Return mock Placemark object. """
    with mock.patch.object(User, "__init__", lambda x: None):
        mock_obj = User()

        return mock_obj


@pytest.mark.parametrize("wardrobes, result", [
    ["1, 2, 3", [1, 2, 3]],
    ["", []],
    ["0, 0, 0, 0", [0, 0, 0, 0]],
    ["1,2 ,3", [1, 2, 3]],
    ["1,2,3", [1, 2, 3]],
    ["1, ", [1]],
    ["1", [1]]
])
def test_get_sanitized_wardrobes(wardrobes, result, user_mock):
    """ Test if wardrobes are correctly formated from method. """
    method_result_wardrobes = user_mock._get_sanitized_wardrobes(wardrobes)

    assert result == method_result_wardrobes


@pytest.mark.parametrize("wardrobe_ids, result", [
    ["1, 2, 3", True],
    ["", False],
    ["1", True],
    ["1,2", True],
    ["1,2,3", True],
    ["1000", False]
])
def test_check_wardrobes(wardrobe_ids, result, user_mock):
    """ Test if funciton tests correctly if a users wardrobe exist. """
    wardrobe_exists = user_mock.check_wardrobes(wardrobe_ids)

    assert wardrobe_exists == result
