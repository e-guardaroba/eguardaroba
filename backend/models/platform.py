"""
Platform model class containing platform classes representing database
models.
"""
# Python libs.
import datetime
import re
from sqlalchemy import (Column, String, Integer, ForeignKey, DateTime,
                        Table, ARRAY, exists)
from sqlalchemy.exc import SQLAlchemyError
# Project files.
from config import CONFIGURATION
from models import Base, session as maria_db
from models.guardrobe import Wardrobe


class User(Base):
    """ DB class representing a user. """
    # Declare table name.
    __tablename__ = 'User'
    # Declare table attributes.
    user_id = Column(Integer, primary_key=True)
    username = Column(String(32))
    password = Column(String(32))
    email = Column(String(32))
    description = Column(String(32))
    user_type = Column(String(32))
    wardrobes = Column(String(32))

    def __init__(self, username, password, email,
                 description, user_type, wardrobes):
        """ Class consructor. """
        self.username = username
        self.password = password
        self.email = email
        self.description = description
        self.user_type = user_type
        self.wardrobes = wardrobes

    def store_to_db(self):
        """ Add new wardrobe to db. """
        # try:
        maria_db.add(self)
        maria_db.commit()
        # except SQLAlchemyError as e:
        # error = str(e.__dict__)
        # print(error)

    @classmethod
    def check_wardrobes(self, wardrobes):
        """ Check if given wardrobes exist. """
        wardrobes_exist = True if wardrobes else False
        for wardrobe_id in self._get_sanitized_wardrobes(wardrobes):
            try:
                (ret, ), = maria_db.query(exists().where(
                    Wardrobe.wardrobe_id == wardrobe_id))
            except ValueError:
                return True
            if not ret:
                wardrobes_exist = False
        return wardrobes_exist

    @classmethod
    def _get_sanitized_wardrobes(self, wardrobes):
        """ Sanitize given wardrobes. """
        sanitized_wardrobes = list()
        for wardrobe in re.split(',', wardrobes):
            try:
                sanitized_wardrobes.append(int(wardrobe.strip()))
            except ValueError:
                pass

        return sanitized_wardrobes


class Post(Base):
    """ DB class representing a post. """
    # Declare table name.
    __tablename__ = 'Post'
    # Declare table attributes.
    post_id = Column(Integer, primary_key=True)
    date = Column(DateTime, default=datetime.datetime.utcnow)
    user_id = Column(Integer)
    title = Column(String(32))
    text = Column(String(1024))
    clothes = Column(String(32))
    images = Column(String(512))
    likes = Column(Integer)
    dislikes = Column(Integer)

    def __init__(self, user_id, title, text, clothes, images, likes, dislikes):
        """ Class consructor. """
        self.user_id = user_id
        self.title = title
        self.text = text
        self.clothes = clothes
        self.images = images
        self.likes = likes
        self.dislikes = dislikes

    def store_to_db(self):
        """ Add new post to db. """
        try:
            maria_db.add(self)
            maria_db.commit()
        except SQLAlchemyError as e:
            error = str(e.__dict__)
            print(error)


class Category(Base):
    """ DB class representing a post. """
    # Declare table name.
    __tablename__ = 'Category'
    # Declare table attributes.
    category_id = Column(Integer, primary_key=True)
    post_id = Column(Integer)
    name = Column(String(32))
    description = Column(String(32))

    def __init__(self, post_id, name, description):
        """ Class consructor. """
        self.post_id = post_id
        self.name = name
        self.description = description

    def store_to_db(self):
        """ Add new category to db. """
        try:
            maria_db.add(self)
            maria_db.commit()
        except SQLAlchemyError as e:
            error = str(e.__dict__)
            print(error)


class Comment(Base):
    """ DB class representing a comment. """
    # Declare table name.
    __tablename__ = 'Comment'
    # Declare table attributes.
    commnet_id = Column(Integer, primary_key=True)
    post_id = Column(Integer)
    user_id = Column(Integer)
    text = Column(String(1024))
    date = Column(DateTime, default=datetime.datetime.utcnow)

    def __init__(self, post_id, user_id, text):
        """ Class consructor. """
        self.post_id = post_id
        self.user_id = user_id
        self.text = text

    def store_to_db(self):
        """ Add new category to db. """
        try:
            maria_db.add(self)
            maria_db.commit()
        except SQLAlchemyError as e:
            error = str(e.__dict__)
            print(error)


class Company(Base):
    """ DB class representing a company. """
    # Declare table name.
    __tablename__ = 'Company'
    # Declare table attributes.
    company_id = Column(Integer, primary_key=True)
    name = Column(String(32))

    def __init__(self, name):
        """ Class consructor. """
        self.name = name

    def store_to_db(self):
        """ Add new category to db. """
        try:
            maria_db.add(self)
            maria_db.commit()
        except SQLAlchemyError as e:
            error = str(e.__dict__)
            print(error)


class Feed(Base):
    """ DB class representing a feed. """
    # Declare table name.
    __tablename__ = 'Feed'
    # Declare table attributes.
    feed_id = Column(Integer, primary_key=True)
    user_id = Column(Integer)
    feed_type = Column(String(32))

    def __init__(self, user_id, feed_type):
        """ Class consructor. """
        self.user_id = user_id
        self.feed_type = feed_type

    def store_to_db(self):
        """ Add new feed to db. """
        try:
            maria_db.add(self)
            maria_db.commit()
        except SQLAlchemyError as e:
            error = str(e.__dict__)
            print(error)


class Ad(Base):
    """ DB class representing an ad. """
    # Declare table name.
    __tablename__ = 'Ad'
    # Declare table attributes.
    ad_id = Column(Integer, primary_key=True)
    ad_number = Column(Integer)
    user_id = Column(Integer)
    time_period = Column(String(32))

    def __init__(self, ad_number, user_id, time_period):
        """ Class consructor. """
        self.ad_number = ad_number
        self.user_id = user_id
        self.time_period = time_period

    def store_to_db(self):
        """ Add new feed to db. """
        try:
            maria_db.add(self)
            maria_db.commit()
        except SQLAlchemyError as e:
            error = str(e.__dict__)
            print(error)


# # HOW TO ADD DATA.
# new_post = Post(user_id=95, title="Pws na nty8eite gia nightout",
#                 text="dummy text", clothes=" ",
#                 images="some_paths", likes=0,
#                 dislikes=0)
# new_post.store_to_db()

# new_category = Category(post_id=1, name="nightout tips",
#                         description="dummy text")
# new_category.store_to_db()

# new_comment = Comment(post_id=1, user_id=95,
#                       text="dummy text")
# new_comment.store_to_db()

# new_company = Company(name="dummy company")
# new_company.store_to_db()

# new_feed = Feed(user_id=1,
#                 feed_type="dummy type")
# new_feed.store_to_db()

# new_ad = Ad(ad_type="type 1", company_id=1, feed_id=1,
#             text="dummy text",
#             image="some path")
# new_ad.store_to_db()
