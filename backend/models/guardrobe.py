"""
Guardaroba model class containing wardrobe classes representing database
models.
"""
# Python libs.
from sqlalchemy import Column, String, Integer, ForeignKey
from sqlalchemy.exc import SQLAlchemyError
# Project files.
from config import CONFIGURATION
from models import Base, session as maria_db


class Wardrobe(Base):
    """ DB class representing a wardrobe. """
    # Declare table name.
    __tablename__ = 'Wardrobe'
    # Declare table attributes.
    wardrobe_id = Column(Integer, primary_key=True)
    user_id = Column(Integer)
    name = Column(String(32))
    description = Column(String(32))
    area = Column(String(32))

    def __init__(self, user_id, name, description, area):
        """ Class consructor. """
        self.user_id = user_id
        self.name = name
        self.description = description
        self.area = area

    def store_to_db(self):
        """ Add new wardrobe to db. """
        try:
            maria_db.add(self)
            maria_db.commit()
        except SQLAlchemyError as e:
            error = str(e.__dict__)
            print(error)


class Cloth(Base):
    """ DB class representing a cloth. """
    # Declare table name.
    __tablename__ = 'Cloth'
    # Declare table attributes.
    cloth_id = Column(Integer, primary_key=True)
    wardrobe_id = Column(Integer, ForeignKey("Wardrobe.wardrobe_id"),
                         nullable=False)
    name = Column(String(32))
    cloth_type = Column(String(32))
    photo = Column(String(1024))

    def __init__(self, wardrobe_id, name, cloth_type, photo):
        """ Class consructor. """
        self.wardrobe_id = wardrobe_id
        self.name = name
        self.cloth_type = cloth_type
        self.photo = photo

    def store_to_db(self):
        """ Store new cloth to db. """
        try:
            maria_db.add(self)
            maria_db.commit()
        except SQLAlchemyError as e:
            error = str(e.__dict__)
            print(error)


class Tag(Base):
    """ DB class representing a tag (of a cloth) """
    # Declare table name.
    __tablename__ = 'Tag'
    # Declare table attributes.
    tag_id = Column(Integer, primary_key=True)
    cloth_id = Column(Integer, ForeignKey("Cloth.cloth_id"),
                      nullable=False)
    name = Column(String(32))

    def __init__(self, cloth_id, name):
        """ Class consructor. """
        self.cloth_id = cloth_id
        self.name = name

    def store_to_db(self):
        """ Add a new tag to a cloth. """
        try:
            maria_db.add(self)
            maria_db.commit()
        except SQLAlchemyError as e:
            error = str(e.__dict__)
            print(error)


class Suitcase(Base):
    """ DB class representing a suitcase (contains cloths) """
    # Declare table name.
    __tablename__ = 'Suitcase'
    # Declare table attributes.
    suitcase_id = Column(Integer, primary_key=True)
    user_id = Column(Integer)
    name = Column(String(32))
    time_period = Column(String(2054))
    occasion = Column(String(32))
    airline = Column(String(32))
    specs = Column(String(32))
    clothes = Column(String(32))

    def __init__(self, user_id, name, time_period,
                 occasion, airline, specs, clothes):
        """ Class consructor. """
        self.user_id = user_id
        self.name = name
        self.time_period = time_period
        self.occasion = occasion
        self.airline = airline
        self.specs = specs
        self.clothes = clothes

    def store_to_db(self):
        """ Add a new tag to a cloth. """
        try:
            maria_db.add(self)
            maria_db.commit()
        except SQLAlchemyError as e:
            error = str(e.__dict__)
            print(error)


class Outfit(Base):
    """ DB class representing a suitcase (contains cloths) """
    # Declare table name.
    __tablename__ = 'Outfit'
    # Declare table attributes.
    outfit_id = Column(Integer, primary_key=True)
    user_id = Column(Integer)
    clothes = Column(String(32))
    name = Column(String(32))
    weather = Column(String(32))

    def __init__(self, clothes, user_id, name,
                 weather):
        """ Class consructor. """
        self.clothes = clothes
        self.user_id = user_id
        self.name = name
        self.weather = weather

    def store_to_db(self):
        """ Create a new outfit """
        try:
            maria_db.add(self)
            maria_db.commit()
        except SQLAlchemyError as e:
            error = str(e.__dict__)
            print(error)


# # HOW TO ADD DATA.
# new_wardrobe = Wardrobe(user_id=419, name="Wardrobe xwrio",
#                         description="rouxa sto xwrio",
#                         area="kalokairina")
# new_wardrobe.store_to_db()


# new_cloth = Cloth(wardrobe_id=1, name="Red tshirt",
#                   cloth_type="T-Shirt", photo="some path")
# new_cloth.store_to_db()


# new_tag = Tag(cloth_id=1,
#               name="xalaro")
# new_tag.store_to_db()

# new_suitcase = Suitcase(user_id=1, name="valitsa 1",
#                         time_period="21062019-27062019",
#                         occasion="vacation",
#                         airline="Etihad",
#                         specs="50cm x 40cm x 25cm - 23kg",
#                         clothes=" ")
# new_suitcase.store_to_db()

# new_outfit = Outfit(clothes=" ", name="outfit 1",
#                     description="my_outfit",
#                     weather="summer")
# new_outfit.store_to_db()
