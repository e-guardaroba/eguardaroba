from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine
from config import CONFIGURATION
""" Return a db object. """
db = create_engine(CONFIGURATION.db_string)
Base = declarative_base()
Session = sessionmaker(db)
session = Session()
