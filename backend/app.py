""" This is the main app module. """
# Python libs.
from flask import Flask
from flask_cors import CORS
# Project files.
from modules import API
from models import Base, db, guardrobe, platform
from config import CONFIGURATION

APP = Flask(__name__)
APP.config['SERVER_NAME'] = CONFIGURATION.get_server_name()
API.init_app(APP)
Base.metadata.create_all(db)
CORS(APP)


# @APP.after_request
# def add_headers(response):
#     """ Add correct headers to response. """
#     response.headers['Access-Control-Allow-Origin'] = ":".join((
#         CONFIGURATION.backend_ip, str(CONFIGURATION.frontend_port)))

#     return response


if __name__ == '__main__':
    APP.run(debug=CONFIGURATION.debug_mode,
            host=CONFIGURATION.backend_ip, port=CONFIGURATION.backend_port)
