# Backend

## Requirements
For downloading the project dependencies you will need [pip](https://pypi.org/project/pip/) and python 3.6.x. A usefull tool for handling different python versions is [pyenv](https://github.com/pyenv/pyenv). Follow the links for more instrunctions on downloading the previous programs.

## Installation
First clone the repository.
```
git clone https://gitlab.com/e-guardaroba/eguardaroba.git
```
To download python dependencies you will need [pipenv](https://pipenv.readthedocs.io/en/latest/).
```
pip install pipenv
```
Navigate into the project folder and run the following command to download the dependencies.
```
cd eguardaroba
pipenv install --dev
```
To run the project using the pip enviroment run:
```
pipenv shell
python app.py
```

## Project Structure
For the project's backend, flask framework was used. Backend/app is the main module, which initiates the server. The rest file structure is explained below.

---
> ### DB
> DB models are developed with sqlalchemy and are implemented under backend/models. \
> The module platform contains all the website-site models and the guardrobe all the app models.

---
> ### API
> Rest api functions are developed under backend/modules folder. Each folder contains a rest_api module, responsible for the api logic and a lib module, responsible for the programming logic of the restful service. All apis are used from a main API router inside modules __init__ module, and then used from app. \
> Also for Visualising the API functions, SWAGGER UI, a fully functional visualisation framework, was used.

---
> ### TESTS
> For unit testing all the needed methods and functions, pythons pytest and unittest libs were used. All unit and functional tests are implemented under backend/tests. For running the test run the following command, inside pip enviroment: \
> ```python -m pytest backend/tests```

---
> ### CONFIGURATION
> Under backend/config there is a template config yaml file which contains all the main configuration for the backend server, frontend and logging mechanism. In odrder for the bakcend to run, copy and paste the file removing __template__ from name.\
> backend/config.py module contains a class which uses the yaml file to create a CONFIGRATION object, with is imported from any module when need to.
